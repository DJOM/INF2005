INF2005 - Travail pratique 1
============================

## Mise en situation

Un nouveau restaurant de sushi vient d'ouvrir dans votre quartier. Le propriétaire de Sushi++ vous demande votre aide pour qu'il puisse accepter les commandes en ligne.

## Spécifications

Le site doit avoir deux interfaces. La première est destinée aux clients qui commandent. L'autre est destinée au cuisinier pour visualiser la liste des commandes.

La page pour les clients doit contenir le logo du restaurant, les coordonnées et le formulaire de commande. Les coordonnées du restaurant sont:

* 2020 rue Futomaki, Montréal (QC) J1J K2K
* Téléphone: 514-123-4567

Le menu du restaurant est le suivant:

* Maki californien 6$
* Maki boston 7$
* Sashimi saumon 4$
* Nigris avocat 3$
* Sushi omelette 2$

Le formulaire doit permettre de choisir la quantité de sushi de chaque type. Le total de la facture doit s'afficher à mesure que les quantités changent. Le client doit aussi indiquer son nom et son numéro de téléphone. Lors de la soumission de la commande, le formulaire doit être validé pour les éléments suivants:

* Le nom comporte au moins 2 caractères
* Le numéro de téléphone comporte 10 chiffres
* Il y a au moins un sushi commandé
* Le nombre de sushis ne peut pas être négatif

Si une des conditions n'est pas remplie, alors l'utilisateur reçoit un message d'erreur lui indiquant les corrections à apporter, mais il ne doit pas perdre les informations qu'il vient de saisir. Lorsque le formulaire est valide, l'utilisateur reçoit un message lui indiquant que sa commande est acceptée et en préparation. Le client peut suivre l'état de sa commande en cliquant sur un bouton d'actualisation. Sa commande est soit "en préparation" ou "prête".

L'interface pour le cuisinier doit afficher la liste de toutes les commandes dans un tableau. Chaque ligne du tableau représente le détail d'une commande, soit:

* Le numéro de commande
* L'état de la commande (en préparation ou prêt)
* La date et l'heure à laquelle le client a passé sa commande
* La date et l'heure à laquelle la commande est devenue prête (vide si la commande n'est pas encore prête)
* Le nom du client
* Le numéro de téléphone du client
* Le nombre de chaque sushi de la commande
* Si la commande est en traitement, un bouton existe pour indiquer que la commande est prête

Lorsque la commande est terminée, le cuisinier clique sur le bouton "prêt" pour indiquer que le client peut venir chercher sa commande. La date et l'heure sont ajoutées à l'information de la commande pour indiquer à quel moment la commande est devenue prête. Les commandes ne sont jamais supprimées du fichier. Par contre, il doit être possible de masquer ou afficher les commandes terminées.

## Réalisation

* Le calcul du total de la facture doit se faire en JavaScript dans le navigateur.
* Le script PHP doit valider les champs du formulaire.
* La soumission du formulaire doit respecter la convention POST-REDIRECT-GET.
* Par simplicité, utilisez un fichier texte de type CSV pour consigner les commandes. Chaque ligne représente une commande et chaque champ est séparé par un point-virgule `;`.
* Utilisez la librairie Semantic-UI pour produire des pages attrayantes. Vous pouvez vous baser sur l'exemple `html/10-ressources`. Vous pouvez utiliser une autre librairie, tel que Bootstrap.
* La page doit contenir le logo du restaurant et une image pour chaque type de sushi. Utilisez les images fournies.

## Pondération

| Élément | Points |
|--       |--      |
| Le formulaire possède les champs requis | 4 |
| Le total de la facture s'affiche correctement | 2 |
| Un formulaire invalide est refusé et une erreur s'affiche | 4 |
| Le formulaire respecte le principe POST-REDIRECT-GET | 2 |
| Le client peut suivre l'état de sa commande | 2 |
| Le tableau des commandes est conforme | 1 |
| Il est possible de marquer une commande prête | 1 |
| Les estampilles de temps sont ajoutées aux informations de la commande | 1 |
| Il est possible de masquer les commandes terminées | 1 |
| L'aspect des pages est professionnel (un style est appliqué à tous les éléments) | 2 |
| Total | 20 |

## Modalités

* Le TP se fait seul ou en équipe de deux.
* Effectuez une archive ZIP contenant tout le code du site.
* Les noms et les matricules des coéquipiers doivent apparaitre dans le fichier AUTEURS.txt.
* La remise se fait par Moodle.
* Ne remettez qu'un fichier ZIP par équipe.
* La date limite de remise est le 23 février 2017 23h59.
