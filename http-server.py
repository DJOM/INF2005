#!/usr/bin/python3

import os
import socket
import time
from http.server import SimpleHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs

hostName = ""
hostPort = 8000

class DummyServer(SimpleHTTPRequestHandler):
	def do_GET(self):
		parsed_path = urlparse(self.path)
		params = parse_qs(parsed_path.query)
		print("url query: %s" % parsed_path.query)
		print("decoded query: %s" % params)
		SimpleHTTPRequestHandler.do_GET(self)

	def do_POST(self):
		content_length = int(self.headers['Content-Length'])
		post_data = self.rfile.read(content_length)
		params = parse_qs(post_data)
		SimpleHTTPRequestHandler.do_GET(self)
		print("post data: %s" % post_data)
		print("decoded query: %s" % params)

myServer = HTTPServer((hostName, hostPort), DummyServer)
print(time.asctime(), "Server Starts - %s:%s" % (hostName, hostPort))

try:
	myServer.serve_forever()
except KeyboardInterrupt:
	pass

myServer.server_close()
print(time.asctime(), "Server Stops - %s:%s" % (hostName, hostPort))
